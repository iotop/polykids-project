<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


<h1>Noise Graph Web App</h1>

<h1> Overview </h1>

<p>Docker create a virtual environment while we setup webapplication. Here, Noise Graph is an laravel web base application. 
We can use any operating system to run docker file but we need setup environment before run the docker file. 
You can <a href="https://docs.docker.com/get-docker/">download docker</a> from base on your operating system. </p>


The Noise Graph Web App was designed to demonstrate noise level in ....
This app was develop in laravel framework and runs itself in laravel framework through Virtual machine on a Dev server.

The Noise Graph App fetch the data from ttn server database under "op_noise" table which private IP address  is which details are as follows:


Whatever data are update in ttn database it fetch all the data. 

<h1> Accessing and Updating The Noise App </h1>
To access the Noise Graph Web App root folder on the virtual machine. Inside that dockerized Noise Web App folder there are two container one 
is “app” which is Noise Graph Web Application, and another is “webserver” where we can expose our Noise graph Web App. 
Firstly, If you need to update Noise Graph Web App you can download Noise graph Web App by git clone or directly download 
from polyKids-project repository and update it an d again need to update dev server Noise graph web server. One thing you 
must remember while you want to update it we need to open PuTTy than enter a server IP address and port number as well as 
username and password that instruction is in Sensitive repository. Here I show you how to run updated Noise Graph Web App
through docker in a web server. 
At the first step you need to open PuTTy and fulfill login credentials, than get inside into given root directory:
- cd /srv/Docker/Noise_app/polykids-project

Once you're in the root directory for the Noise Graph Web App, you can use git to update the files to the latest version.
git pull
Once you've pulled any required files from the repository, you will need to update the current build within the container. 
This is achieved with the following commands:

- sudo docker-compose build
- sudo docker-compose up 

When you run the build command, you will see output like this:
 
 ![directory Image]( images/docker_build_view.jpg)

If your “sudo docker-compose build” is successfully run you can run “sudo docker-compose up”.

For running Noise Graph Web App Docker Container on background continuously you need this command:

- sudo docker-compose up -d

As a final step, visit http://10.25.138.157:8080/noise-payload in the browser. 
Only If you get permission denied problem you can give permission by using this command:<br>
- student@ubuntu:/srv/Docker/Noise_app/polykids-project$<i> sudo chmod -R 777 storage/ </i>

You will see the Noise Graph page on your browser like this:
![directory Image](images/server_output.PNG)

<h1> To Running Locally “Noise Graph Web App” </h1>
To run locally Noise Graph Web App, you need to setup Laravel environment. First you need to Xampp server and Laravel composer
to run this App. The installation instructions for it can be found Below:
Note: First you need to download Xampp server than only you can able to download Laravel composer.
You can <a href="https://www.apachefriends.org/download.html"> Download Xampp Server</a> and <a href="https://getcomposer.org/"> Laravel Composer </a>
base on your operating system. <br>


Once you've downloaded and installed Xampp Server and Laravel Composer than only you can run Noise Graph Web App Locally.
Remember one thing when you try to run locally you must put that file inside <i>cd \xampp\htdocs>cd polykids-project-laravel_Noisegraph_Docker</i>
and don't forget to add .env file from sensitive repository.
![directory Image](images/cmdView1.jpg)
 
Here you need to generate app key, config cache and clear config. You can run this command on command line:
- php artisan key:generate
- php artisan config:cache
- php artisan config:clear


As a final step, run php artisan serve visit  http://127.0.0.1:8000/noise-payload in the browser. You will see the Noise Graph page 
on your browser.
 ![directory Image]( images/localArtasianServe.jpg)
Final Output while running locally is like this:
 
![directory Image]( images/local_server_output.PNG)



<h1>Summary</h1>
<p> while install docker and docker composer we need to install in a computer not inside a noisegraph folder.
So first install docker and docker composer in your machine by following given command below and thank only get 
inside your webapplication folder i.e Noisegraph </p>
<b>	 Important Notes:</b> Plesae add .env file from given link below to conect with ttn server database. It is in sensitive repository:
https://gitlab.com/iotop/sensitive/-/blob/master/Noise%20Graph%20Web%20App/.env

     Install docker  with 'sudo apt install docker.io' 
     install docker-compose with 'sudo apt install docker-compose'
	 run sudo systemctl start docker. (This command start your docker in your machine)
	 run sudo systemctl enable docker. (This command enable your docker in your machine)
	 run docker --version' and 'docker run hello-world' to verify you have installed docker correctly.
	 Navigate to the NoiseGraph webapplication then-> for example "c:NoiseGraph/run sudo docker-compose up -d".
	 In same way run given below command inside Noisegraph web app directory.
	 run sudo docker-compose up -d 
	 run sudo docker-compose exec app php artisan key:generate. (This command generate App key of our Noise Graph web app)
	 run sudo docker-compose exec app php artisan config:cache
	 run sudo docker-compose exec app php artisan config:clear
 
	 As a final step, visit http://your_server_ip/noise-payload in the browser. You will see the Noise Graph graph page on your browser.

	
	Thank You

	
