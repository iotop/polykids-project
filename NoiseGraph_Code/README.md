<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>
Documentation of how noise level graph file run our local machine.

Step 1: 
Install xampp server which local server on c drive(you can install in any drive) it install PHP in your computer.
Check Php version it must equal or greater than PHP version is 7.2.27 
Check in Cmd by typing php -v. 
<br>
Step 2
Install Laravel composer (version 6X) in c drive (you can install in any drive). Composer version 1.9.3
After install composer you can check in Cmd by typing composer.
<br>
Step 3
Then after install npm in your local machine.
 <br>
Step 4
Keep your Laravel file inside xampp/htdocs directory (eg c: xampp/htdocs/Your_File).<br>
Step 5
Open xampp server which you already install it before
Than start Apacha server and  start Mysql.<br>
 
Step 6
Create a database inside localhost/phpMyAdmin create whatever database name you like to create (eg: noisegraph) then run a migration command in command line. 
<br>

Step 7
Open  noiseGraph_code folder rename .env example file name by .env. Thanm now open noiseGraph_code folder in text editor (like visual studio code, notepad++) than open .env file in text editor check your database connection DB_Database= your_database or not.
 <br>
Remember one important notice due to security reason when we upload our Laravel file on gitlab/github .env file and .sql file and app key is not uploaded. So we must generate Key first to run Laravel file into our browser.
So run a command from project directory first to generate app key type  php artisan key:generate 
Eg: C:\xampp\htdocs\NoiseGraph_Code> php artisan key:generate<br>
			(Only if )<br>
Note : If key is not generate you need to update your Laravel composer. For that run composer update command
Eg: C:\xampp\htdocs\NoiseGraph_Code> composer update
<br>
Step 8:
After that run a migration command to migrate database file into database. (Remember it will migrate into mysql database)
Eg: C:\xampp\htdocs\NoiseGraph_Code> php artisan migrate
So you need to Import noise_level.sql table into your_Database (eg: import noise_level.sql inside noisegraph database)
<br>
Step:9
Type C:\xampp\htdocs\NoiseGraph_Code than type php artisan serve to run server:<br>
It will create serve URL: http://127.0.0.1:8000<br>
 To see Noise level graph open browser hit http://127.0.0.1:8000/noise-payload Please do not forget <u>/ noise-payload</u>
<br>
Thank You

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)
- [Appoly](https://www.appoly.co.uk)
- [OP.GG](https://op.gg)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
