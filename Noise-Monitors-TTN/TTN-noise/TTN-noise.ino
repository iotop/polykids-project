/************************** Configuration ***********************************/
#include <TinyLoRa.h>
#include <SPI.h>

// Visit your thethingsnetwork.org device console
// to create an account, or if you need your session keys.

// Device Address (MSB)
uint8_t DevAddr[4] = { **** };

// Network Session Key (MSB)
uint8_t NwkSkey[16] = { **** };

// Application Session Key (MSB)
uint8_t AppSkey[16] = { **** };

/************************** Begins Here ***********************************/

const int MIC = 0; //the microphone amplifier output is connected to pin A0
int adc;
int dB; //the variable that will hold the value read from the microphone each time

// because max value is 100, s
unsigned char payload[3];


// How many times data transfer should occur, in seconds
//const unsigned int sendInterval = 10;

// Pinout for Adafruit Feather 32u4 LoRa
TinyLoRa lora = TinyLoRa(7, 8, 4);

// Pinout for Adafruit Feather M0 LoRa
//TinyLoRa lora = TinyLoRa(3, 8, 4);


void setup()
{
  delay(2000);
  Serial.begin(9600);
  while (! Serial);

  // Initialize pin LED_BUILTIN as an output
  pinMode(LED_BUILTIN, OUTPUT);

  // Initialize LoRa
  Serial.print("Starting LoRa...");

  // define multi-channel sending
  lora.setChannel(MULTI);

  // set datarate
  lora.setDatarate(SF7BW125);
  if (!lora.begin())
  {
    Serial.println("Failed");
    Serial.println("Check your radio");
    while (true);
  }

  // Optional set transmit power. If not set default is +17 dBm.
  // Valid options are: -80, 1 to 17, 20 (dBm).
  // For safe operation in 20dBm: your antenna must be 3:1 VWSR or better
  // and respect the 1% duty cycle.

  lora.setPower(1);

  Serial.println("OK");
}

void loop()
{

  adc = analogRead(MIC); //Read the ADC value from amplifer
  dB = (adc + 83.2073) / 11.003; //Convert ADC value to dB using Regression values


  //convert int to string
  String str;
  str = String(dB);
  str.toCharArray(payload, 3);


  Serial.println("Sending LoRa Data...");
  lora.sendData(payload, sizeof(payload), lora.frameCounter);
  Serial.print("Frame Counter: "); Serial.println(lora.frameCounter);
  lora.frameCounter++;


  // blink LED to indicate packet sent
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.println("delaying...");

  Serial.print("str is : ");
  Serial.println(str);

  Serial.println("-----------");

  //send to TTN every 30 seconds;
  delay(30000);



}
