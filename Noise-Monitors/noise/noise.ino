const int MIC = 0; //the microphone amplifier output is connected to pin A0
int adc;
int dB, PdB; //the variable that will hold the value read from the microphone each time

void setup() {
Serial.begin(9600); //sets the baud rate at 9600 so we can check the values the microphone is obtaining on the Serial Monitor
  pinMode(11, OUTPUT); //Red LED
  pinMode(12, OUTPUT); //Yellow LED
  pinMode(13, OUTPUT); //Greend LED
}

void loop(){

  PdB = dB; //Store the previous of dB here
  
adc= analogRead(MIC); //Read the ADC value from amplifer 
dB = (adc+83.2073) / 11.003; //Convert ADC value to dB using Regression values

if (PdB!=dB) // If the Decibel changed
Serial.println (dB);

if (dB<61)
{
  digitalWrite(13, HIGH);   // turn the Green LED on (HIGH is the voltage level)
  delay(1000);                       // Keep it on if DB lower than 70
  digitalWrite(13, LOW); 
}

else if(dB>62)
{
  digitalWrite(11, HIGH);   // turn the Red LED on (HIGH is the voltage level)
  delay(1000);                       // wait for ten seconds
  digitalWrite(11, LOW); 
}

else
{
  digitalWrite(12, HIGH);   // turn the Yellow LED on (HIGH is the voltage level)
  delay(1000);                       // wait for ten seconds
  digitalWrite(12, LOW); 
  }

}
