

## Noise monitor 

A sound detector to monior the noise in kindergarden

The indicator light should be keep green when the decibel lower than 70.

And will turn yellow when the decibel between 70 and 85.

And will turn red when the decibel higher than 85..

## Schematic
![Schematic](https://gitlab.com/iotop/polykids-project/-/raw/master/image/Schematic.png)

## Introduction 

Here is a video that introducate the noisy monitor by myself.
You can know the all function in this [VIDEO](https://youtu.be/pdFCACDtebo).

## How to build the monitor

### Component

| Name | Number |
|:---:|:---:|
|320 Ω Resistor|Three|
|LED(Red,Green,Yellow)|One for each|
|Prototype Board|One|
|Sound Detector|One|
|Arduino Nano|One|
|Wires|Some|

Tip: I use GY MAX 4466 for my sound detector
You can find cheetsheet [HERE](https://gitlab.com/iotop/polykids-project/-/blob/master/MAX4466.pdf)
)

The Schematic should like this
(Didn't find prototype board in the Fritzing, so I use breadboard)
![Schematic](https://gitlab.com/iotop/polykids-project/-/raw/master/image/noise2.jpg)

### The case

I use [ThinkerCAD](https://www.tinkercad.com) to design the Monitor case.

There are holes in the top of the case that allow the sound detector to get a wider range of sound and make the LEDs out of the case.
![case-top](https://gitlab.com/iotop/polykids-project/-/raw/master/image/case-top.png)

The bottom part has a small edge that can keep the Arduino board stable and has two holes on each side, it can use the USB cable to connect the USB port or use the power bank if the user wants to.
![case-bottom](https://gitlab.com/iotop/polykids-project/-/raw/master/image/case-bottom.png)

### The power

I use four AA bettery to support the monitor, the Battery Capacity is 2700mAh, depend my monitor average current consumption is 35mAh, four bettery should work around 75 hours.

The [Detail](https://snappy.co.nz/products/1-5v-aa-alkaline-batteries-4-pack?gclid=CjwKCAjwlovtBRBrEiwAG3XJ-149xjitymzm_numnjQQCC3R7O4LFCOLVpfocMDf86uSflhSdMnshBoCZlkQAvD_BwE) about the bettery.

## The code


```C
const int MIC = 0; //the microphone amplifier output is connected to pin A0
int adc;
int dB, PdB; //the variable that will hold the value read from the microphone each time

void setup() {

  Serial.begin(9600); //sets the baud rate at 9600 so we can check the values the microphone is obtaining on the Serial Monitor
  pinMode(3, OUTPUT); //Greend LED
  pinMode(5, OUTPUT); //Yellow LED
  pinMode(7, OUTPUT); //Red LED

}

void loop(){

  PdB = dB; //Store the previous of dB here
  
adc= analogRead(MIC); //Read the ADC value from amplifer 
dB = (adc+83.2073) / 11.003; //Convert ADC value to dB using Regression values

if (PdB!=dB) // If the Decibel changed
Serial.println (dB);

if (dB<70)
{

  digitalWrite(7, HIGH);   // turn the Green LED on (HIGH is the voltage level)
  delay(500);                       // Keep it on if DB lower than 70
  digitalWrite(7, LOW); 

}

else if(dB>85)
{
  digitalWrite(3, HIGH);   // turn the Red LED on (HIGH is the voltage level)
  delay(10000);                       // wait for ten seconds
  digitalWrite(3, LOW); 
}
else


{
  digitalWrite(5, HIGH);   // turn the Yellow LED on (HIGH is the voltage level)
  delay(10000);                       // wait for ten seconds
  digitalWrite(5, LOW); 
 }

  }
```
